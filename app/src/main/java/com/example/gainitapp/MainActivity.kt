package com.example.gainitapp

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.outlined.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController
import com.example.gainitapp.ui.theme.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.gainitapp.ui.theme.home.HomeScreen

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

//    private val viewModel by viewModels<MainViewModel>()
    lateinit var navController: NavHostController

    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
//            val context = LocalContext.current
//            val exercises = viewModel.exercises.collectAsState()

            GainItAppTheme {
                Scaffold () {
                    navController = rememberNavController()
                    SetupNetGraph(navController)
                }
//                val navController = rememberNavController()
//                NavHost(navController = navController, startDestination = Screen.HomeScreen.route) {
//                    // HOME
//                    composable(route = Screen.HomeScreen.route) {
//                        HomeScreen(navController = navController)
//                    }
//
//                    // WORKOUTS
//                    composable(route = Screen.WorkoutsScreen.route) {
//                        WorkoutsScreen()
//                    }
//                }

//                NavHost(navController = navController, startDestination = "home") {
//                    navigation(
//                        startDestination = "login",
//                        route = "auth"
//                    ) {
//                        composable("login") {
//                            Button(onClick = {
//                                navController.navigate("home") {
//                                    popUpTo("auth") {
//                                        inclusive = true
//                                    }
//                                }
//                            }) {
//
//                            }
//
//                        }
//                        composable("register") {
//
//                        }
//                        composable("forgot_password") {
//
//                        }
//                    }
//                }

//                Surface(
//                    modifier = Modifier.fillMaxSize(),
//                    color = MaterialTheme.colorScheme.background
//                ) {
//                    NavDrawer()
//                }

//                exercises.value?.let {
//                    when(it) {
//                        is Resource.Failure -> {
//                            Toast.makeText(context, it.exception.message!!, Toast.LENGTH_SHORT).show()
//                        }
//                        Resource.Loading -> {
//                            //@todo loading bar or something
//                        }
//                        is Resource.Success -> {
//                            ExerciseList(it.result)
//                        }
//
//                        else -> {}
//                    }
//                }

            }
        }
    }
}

@Composable
inline fun <reified T :ViewModel> NavBackStackEntry.sharedViewModel(navController: NavController): T {
    val navGraphRoute = destination.parent?.route ?: return viewModel() // it should be hilt viewModel here
    val parentEntry = remember(this) {
        navController.getBackStackEntry(navGraphRoute)
    }
    return viewModel(parentEntry)
}

