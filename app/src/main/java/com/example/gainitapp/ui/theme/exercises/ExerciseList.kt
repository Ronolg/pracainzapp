package com.example.gainitapp.ui.theme.exercises


import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.gainitapp.data.remote.model.Exercise

@Composable
 fun ExerciseList(exercises: List<Exercise>?) {

    val nonNullableExercises = exercises ?: emptyList()

    LazyColumn {
        items(nonNullableExercises) {
            exercise ->
            ExerciseItem(exercise = exercise)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewExerciseList() {
    val exercises = listOf(
        Exercise("Exercise 1", "Description for Exercise 1"),
        Exercise("Exercise 2", "Description for Exercise 2"),
        Exercise("Exercise 3", "Description for Exercise 3")
    )

    ExerciseList(exercises)
}