package com.example.gainitapp.ui.theme.exercises

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.model.Exercise
import com.example.gainitapp.ui.theme.MainViewModel
import com.example.gainitapp.ui.theme.NavigationDrawer

@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("StateFlowValueCalledInComposition", "UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ExercisesScreen(navController: NavHostController, viewModel: MainViewModel) {
    val exercisesState = viewModel.getExercises().collectAsState()
    var showAddExercisePopup by remember { mutableStateOf(false) }
    var newExerciseName by remember { mutableStateOf("") }
    var newExerciseDescription by remember { mutableStateOf("") }

    NavigationDrawer(navController) {
        Scaffold (
            floatingActionButton = {
                FloatingActionButton(
                    onClick = { showAddExercisePopup = true },
                    containerColor = MaterialTheme.colorScheme.primaryContainer,
                    contentColor = MaterialTheme.colorScheme.primary,
                    shape = CircleShape,
                ) {
                    Icon(Icons.Filled.Add, "Add new exercise")
                }
            },
            floatingActionButtonPosition = FabPosition.End
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                    contentAlignment = Alignment.Center
                ) {
                    Text(text = "Exercises", fontSize = 20.sp)
                }

                when (exercisesState.value) {
                    is Resource.Success -> {
                        val exercises = (exercisesState.value as Resource.Success<List<Exercise>>).result
                        Box(modifier = Modifier.fillMaxWidth()) {
                            ExerciseList(exercises)
                        }
                    }
                    is Resource.Failure -> {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth(),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(text = "No exercises found", fontSize = 16.sp)
                        }
                    }

                    Resource.Loading -> {}

                    null -> {}
                }

                if (showAddExercisePopup) {
                    AlertDialog(
                        onDismissRequest = { showAddExercisePopup = false },
                        title = { Text(text = "Add new exercise") },
                        text = {
                            Column(
                                modifier = Modifier
                                    .padding(8.dp)) {

                                OutlinedTextField(
                                    value = newExerciseName,
                                    onValueChange = { newExerciseName = it },
                                    label = { Text("Exercise name") },
                                    modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp)
                                )

                                OutlinedTextField(
                                    value = newExerciseDescription,
                                    onValueChange = { newExerciseDescription = it },
                                    label = { Text("Exercise description") },
                                    modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp)
                                )
                            }
                        },
                        dismissButton = {
                            Button(
                                onClick = {
                                    showAddExercisePopup = false
                                }
                            ) {
                                Text("Cancel")
                            }
                        },
                        confirmButton = {
                            Button(
                                onClick = {
                                    showAddExercisePopup = false
                                    viewModel.addExercise(Exercise(newExerciseName, newExerciseDescription))
                                }
                            ) {
                                Text("Add")
                            }
                        },
                        modifier = Modifier
                            .width(300.dp)
                    )
                }
            }
        }
    }
}
