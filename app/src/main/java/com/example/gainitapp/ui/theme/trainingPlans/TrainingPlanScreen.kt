package com.example.gainitapp.ui.theme.trainingPlans

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.model.TrainingPlan
import com.example.gainitapp.ui.theme.MainViewModel
import com.example.gainitapp.ui.theme.NavigationDrawer

@Composable
fun TrainingPlanScreen(navController: NavHostController, viewModel: MainViewModel, trainingPlanId: Long?) {

    val trainingPlanState = trainingPlanId?.let { viewModel.getTrainingPlanById(it).collectAsState() }

    NavigationDrawer(navController) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
                .verticalScroll(rememberScrollState())
        ) {
            if (trainingPlanState != null) {
                when (trainingPlanState.value) {
                    is Resource.Success -> {
                        val trainingPlan = (trainingPlanState.value as Resource.Success<TrainingPlan>).result

                        var dayNumber = 1
                        trainingPlan.trainingDays.forEach {
                            trainingDay -> TrainingDayItem(trainingDay, dayNumber)
                            dayNumber++
                        }
                    }

                    is Resource.Failure -> {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth(),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(text = "Training plan was not found", fontSize = 16.sp)
                        }
                    }

                    Resource.Loading -> {}

                    null -> {}
                }
            }
        }
    }

}