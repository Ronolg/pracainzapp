package com.example.gainitapp.ui.theme.trainingPlans

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import com.example.gainitapp.data.remote.model.TrainingPlan
import com.example.gainitapp.ui.theme.MainViewModel
import com.example.gainitapp.ui.theme.workouts.WorkoutItem

@Composable
fun TrainingPlanList(navController : NavHostController, trainingPlans: List<TrainingPlan>, viewModel: MainViewModel) {
    LazyColumn {
        items(trainingPlans) {
                trainingPlan ->
            TrainingPlanItem(navController, trainingPlan, viewModel)
        }
    }
}
