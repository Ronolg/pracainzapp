package com.example.gainitapp.ui.theme.workouts

import android.annotation.SuppressLint
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.gainitapp.data.mutable.MutableSet
import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.model.UserExercise
import com.example.gainitapp.data.remote.model.Workout
import com.example.gainitapp.ui.theme.MainViewModel
import com.example.gainitapp.ui.theme.NavigationDrawer
import com.example.gainitapp.ui.theme.Screen
import com.example.gainitapp.ui.theme.userexercises.UserExerciseItem
import com.example.gainitapp.ui.theme.userexercises.UserExerciseList
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter", "StateFlowValueCalledInComposition")
@Composable
fun AddWorkoutScreen(navController: NavHostController, viewModel: MainViewModel) {
    var confirmCreationPopup by remember { mutableStateOf(false) }

    var userExercises = remember {
        mutableStateListOf<UserExercise>()
    }

    var showAddExercisePopup by rememberSaveable {
        mutableStateOf(false)
    }

    var showAddSetPopup by rememberSaveable {
        mutableStateOf(false)
    }

    NavigationDrawer(navController) {
        Scaffold (
            bottomBar = {
                Row (
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(32.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                )
                {
                    FloatingActionButton(
                        onClick = { showAddExercisePopup = true },
                        containerColor = MaterialTheme.colorScheme.primaryContainer,
                        contentColor = MaterialTheme.colorScheme.primary,
                        shape = CircleShape,
                    ) {
                        Text(text = "Add new exercise", modifier = Modifier.padding(vertical = 16.dp, horizontal = 32.dp))
                    }

                    FloatingActionButton(
                        onClick = { confirmCreationPopup = true },
                        containerColor = MaterialTheme.colorScheme.primaryContainer,
                        contentColor = MaterialTheme.colorScheme.primary,
                        shape = CircleShape,
                    ) {
                        Icon(Icons.Filled.Check, "Log this workout")
                    }
                }
            }
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)
                    .verticalScroll(rememberScrollState())
            ) {
                userExercises.forEach { userExercise ->
                    Card(
                        modifier = Modifier
                            .padding(4.dp)
                            .fillMaxWidth()
                            .clickable {
                                showAddSetPopup = true
                            },
                        colors = CardDefaults.cardColors(
                            containerColor = MaterialTheme.colorScheme.primaryContainer
                        )
                    ) {
                        Row(
                            horizontalArrangement = Arrangement.SpaceBetween
                        ) {
                            Box(modifier = Modifier
                                    .align(Alignment.CenterVertically)
                                    .padding(vertical = 4.dp)) {
                                Icon(
                                    Icons.Filled.Add,
                                    contentDescription = "Edit exercise",
                                    tint = MaterialTheme.colorScheme.primary
                                )
                            }

                            UserExerciseItem(userExercise)
                        }
                    }

                    if(showAddSetPopup) {
                        var mutableSet = MutableSet()

                        AlertDialog(
                            onDismissRequest = { showAddSetPopup = false },
                            title = { Text(text = "Add new set") },
                            text = {
                                Column(
                                    modifier = Modifier
                                        .padding(8.dp)) {

                                    Row(
                                        modifier = Modifier.fillMaxWidth(),
                                        horizontalArrangement = Arrangement.SpaceBetween
                                    ) {
                                        OutlinedTextField(
                                            value = mutableSet.reps,
                                            onValueChange = { mutableSet.reps = it },
                                            label = { Text("Reps") },
                                            modifier = Modifier
                                                .padding(vertical = 8.dp)
                                                .weight(1f),
                                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                                            shape = CircleShape
                                        )

                                        Spacer(modifier = Modifier.width(8.dp))

                                        OutlinedTextField(
                                            value = mutableSet.weight,
                                            onValueChange = { mutableSet.weight = it },
                                            label = { Text("Weight") },
                                            modifier = Modifier
                                                .padding(vertical = 8.dp)
                                                .weight(1f),
                                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                                            shape = CircleShape
                                        )
                                    }

                                    OutlinedTextField(
                                        value = mutableSet.technique,
                                        onValueChange = { mutableSet.technique = it },
                                        label = { Text("Technique") },
                                        modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp),
                                        shape = CircleShape
                                    )

                                    OutlinedTextField(
                                        value = mutableSet.note,
                                        onValueChange = { mutableSet.note = it },
                                        label = { Text("Note") },
                                        modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp),
                                        shape = CircleShape
                                    )
                                }
                            },
                            dismissButton = {
                                Button(
                                    onClick = {
                                        showAddSetPopup = false
                                    }
                                ) {
                                    Text("Cancel")
                                }
                            },
                            confirmButton = {
                                Button(
                                    onClick = {
                                        if(mutableSet.weight.isNotEmpty() and mutableSet.reps.isNotEmpty()) {
                                            showAddSetPopup = false
                                            userExercise.sets.add(mutableSet.toNonMutable())
                                        }
                                    },
                                    colors = ButtonDefaults.buttonColors(Color.Green)
                                ) {
                                    Icon(
                                        imageVector = Icons.Filled.Check,
                                        contentDescription = "Change",
                                        tint = Color.White
                                    )
                                }
                            },
                            modifier = Modifier
                                .width(300.dp)
                        )
                    }
                }

                if(userExercises.isEmpty()) {
                    Row (
                        modifier = Modifier
                            .padding(32.dp)
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = "No exercises were added yet,\n click below to do so.",
                            fontSize = 16.sp,
                            textAlign = TextAlign.Center,
                            maxLines = 2
                        )
                    }
                }

                if(showAddExercisePopup) {
                    var newExerciseName by remember { mutableStateOf("") }

                    AlertDialog(
                        onDismissRequest = { showAddExercisePopup = false },
                        title = { Text(text = "Add new exercise", color = MaterialTheme.colorScheme.primary)},
                        text = {
                            Column(
                                modifier = Modifier
                                    .padding(8.dp)) {

                                OutlinedTextField(
                                    value = newExerciseName,
                                    onValueChange = { newExerciseName = it },
                                    label = { Text("Exercise name") },
                                    modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp),
                                    shape = CircleShape
                                )
                            }
                        },
                        dismissButton = {
                            Button(
                                onClick = {
                                    showAddExercisePopup = false
                                }
                            ) {
                                Text("Cancel")
                            }
                        },
                        confirmButton = {
                            Button(
                                onClick = {
                                    showAddExercisePopup = false
                                    userExercises.add(UserExercise(name = newExerciseName))
                                }
                            ) {
                                Text("Add")
                            }
                        },
                        modifier = Modifier
                            .width(300.dp)
                    )
                }

                if(confirmCreationPopup) {
                    var workoutNote by remember { mutableStateOf("")}

                    AlertDialog(
                        onDismissRequest = { confirmCreationPopup = false },
                        title = { Text(text = "Log workout") },
                        text = {
                            OutlinedTextField(
                                value = workoutNote,
                                onValueChange = {workoutNote = it },
                                label = { Text("Workout note") },
                                modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp),
                                shape = CircleShape
                            )
                        },
                        dismissButton = {
                            Button(
                                onClick = {
                                    confirmCreationPopup = false
                                },
                                colors = ButtonDefaults.buttonColors(Color.Red)
                            ) {
                                Icon(
                                    imageVector = Icons.Filled.Close,
                                    contentDescription = "Cancel",
                                    tint = Color.White
                                )
                            }
                        },
                        confirmButton = {
                            Button(
                                onClick = {
                                    val currentDate: LocalDate = LocalDate.now()
                                    val formattedDate: String = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))

                                    confirmCreationPopup = false

                                    viewModel.addWorkout(
                                        Workout(
                                            date = formattedDate,
                                            note = workoutNote,
                                            userExercises = userExercises
                                        ).toDto()
                                    )

                                    navController.navigate(Screen.WorkoutsScreen.route)
                                },
                                colors = ButtonDefaults.buttonColors(Color.Green)
                            ) {
                                Icon(
                                    imageVector = Icons.Filled.Check,
                                    contentDescription = "Log",
                                    tint = Color.White
                                )
                            }
                        },
                        modifier = Modifier
                            .width(300.dp)
                    )
                }
            }
        }
    }
}
