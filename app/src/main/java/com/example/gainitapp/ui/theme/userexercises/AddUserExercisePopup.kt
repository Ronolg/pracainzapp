package com.example.gainitapp.ui.theme.userexercises

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.example.gainitapp.data.remote.model.Set
import com.example.gainitapp.data.remote.model.TrainingDay
import com.example.gainitapp.data.remote.model.UserExercise

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddUserExercisePopup(trainingDay: TrainingDay,  onDismissRequest: () -> Unit) {
    var expanded by remember { mutableStateOf(false) }

    var userExercise = remember {
        mutableStateOf(UserExercise())
    }

    var sets = remember {
        mutableStateListOf<Set>()
    }

    Dialog(
        onDismissRequest = { onDismissRequest() }
    ) {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .height(375.dp)
                .padding(16.dp)
                .verticalScroll(rememberScrollState()),
            shape = RoundedCornerShape(16.dp),
            colors = CardDefaults.cardColors(
                contentColor = MaterialTheme.colorScheme.primaryContainer
            )
        ) {

            OutlinedTextField(
                value = userExercise.value.name,
                onValueChange = { userExercise.value.name = it },
                label = { Text("Exercise name") },
                modifier = Modifier
                    .padding(vertical = 8.dp),
                shape = CircleShape
            )

//            Button(
//                onClick =  { sets.add(Set()) }
//            ) {
//                Icon(
//                    Icons.Filled.Add,
//                    contentDescription = "Add new set",
//                    tint = MaterialTheme.colorScheme.primary
//                )
//            }
//
//            Divider(
//                color = MaterialTheme.colorScheme.primary,
//                thickness = 1.dp,
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .height(1.dp)
//                    .padding(horizontal = 16.dp)
//            )
//
//            sets.forEach { set ->
//                Card(
//                    colors = CardDefaults.cardColors(
//                        contentColor = MaterialTheme.colorScheme.primaryContainer
//                    ),
//                    modifier = Modifier
//                        .padding(4.dp)
//                        .clickable { expanded = !expanded },
//                ) {
//                    Text("SET")
//
//                    if (expanded) {
//                        OutlinedTextField(
//                            value = userExercise.value.name,
//                            onValueChange = { userExercise.value.name = it },
//                            label = { Text("Set") },
//                            modifier = Modifier
//                                .fillMaxWidth()
//                                .padding(vertical = 8.dp),
//                            shape = CircleShape
//                        )
//                    }
//                }
//            }
        }
    }
}