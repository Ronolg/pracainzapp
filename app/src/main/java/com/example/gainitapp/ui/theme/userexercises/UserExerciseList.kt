package com.example.gainitapp.ui.theme.userexercises

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.*
import com.example.gainitapp.data.remote.model.UserExercise

@Composable
fun UserExerciseList(userExercises: List<UserExercise>) {
    userExercises.forEach { userExercise ->
        UserExerciseItem(userExercise)
    }
}
