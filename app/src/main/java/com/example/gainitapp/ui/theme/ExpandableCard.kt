package com.example.gainitapp.ui.theme

import android.annotation.SuppressLint
import androidx.compose.animation.core.*
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.material3.R
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

//@Composable
//fun CardArrow(
//    degrees: Float,
//    onClick: () -> Unit
//) {
//    IconButton(
//        onClick = onClick,
//        content = {
//            Icon(
//                painter = painterResource(id = R.drawable.ic_expand_less_24),
//                contentDescription = "Expandable Arrow",
//                modifier = Modifier.rotate(degrees),
//            )
//        }
//    )
//}
//
//@Composable
//fun CardTitle(title: String) {
//    Text(
//        text = title,
//        modifier = Modifier
//            .fillMaxWidth()
//            .padding(16.dp),
//        textAlign = TextAlign.Center,
//    )
//}

@SuppressLint("UnusedTransitionTargetStateParameter")
@Composable
fun ExpandableCard(
    onCardArrowClick: () -> Unit,
    expanded: Boolean,
) {

    Card(
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        ),
//        shape = RoundedCornerShape(cardRoundedCorners),
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = 8.dp,
                vertical = 8.dp
            )
    ) {
        Column {
            Box {
//                CardArrow(
//                    degrees = arrowRotationDegree,
//                    onClick = onCardArrowClick
//                )
//                CardTitle(title = card.title)
            }

            Text(
                text = "Expandable content here",
                textAlign = TextAlign.Center
            )
        }
    }
}