package com.example.gainitapp.ui.theme.trainingPlans

import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.gainitapp.data.mutable.MutableSet
import com.example.gainitapp.data.remote.model.TrainingDay
import com.example.gainitapp.ui.theme.userexercises.UserExerciseItem
import com.example.gainitapp.ui.theme.userexercises.UserExerciseList

@Composable
fun TrainingDayItem(trainingDay: TrainingDay, dayNumber: Int) {
    val interactionSource = remember {
        MutableInteractionSource()
    }
    Box(
        modifier = Modifier
            .indication(interactionSource, LocalIndication.current)
            .padding(8.dp)
    ) {
        Text(text = "Day $dayNumber", fontSize = 20.sp, color = MaterialTheme.colorScheme.primary)
    }

    UserExerciseList(trainingDay.userExercises)
    Spacer(modifier = Modifier.height(16.dp))
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TrainingDayItemCreator(trainingDay: TrainingDay, dayNumber: Int, showAddExercisePopup: () -> Unit = {}) {
    val interactionSource = remember {
        MutableInteractionSource()
    }

    Box(
        modifier = Modifier
            .indication(interactionSource, LocalIndication.current)
            .padding(8.dp)
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Text(text = "Day $dayNumber", fontSize = 20.sp, color = MaterialTheme.colorScheme.primary)

            Spacer(modifier = Modifier.width(16.dp))

            Button(
                onClick = { showAddExercisePopup() },
                colors = ButtonDefaults.buttonColors(
                    containerColor = MaterialTheme.colorScheme.primaryContainer
                ),
                shape = CircleShape
            ) {
                Text(text = "Add exercise", color = MaterialTheme.colorScheme.primary)
            }
        }
    }


    var showAddSetPopup by rememberSaveable {
        mutableStateOf(false)
    }

    trainingDay.userExercises.forEach { userExercise ->
        Card(
            modifier = Modifier
                .padding(4.dp)
                .fillMaxWidth()
                .clickable {
                    showAddSetPopup = true
                },
            colors = CardDefaults.cardColors(
                containerColor = MaterialTheme.colorScheme.primaryContainer
            )
        ) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Box(modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .padding(vertical = 4.dp)) {
                    Icon(
                        Icons.Filled.Add,
                        contentDescription = "Edit exercise",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }

                UserExerciseItem(userExercise)
            }
        }

        if(showAddSetPopup) {
            var mutableSet = MutableSet()

            AlertDialog(
                onDismissRequest = { showAddSetPopup = false },
                title = { Text(text = "Add new set") },
                text = {
                    Column(
                        modifier = Modifier
                            .padding(8.dp)) {

                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween
                        ) {
                            OutlinedTextField(
                                value = mutableSet.reps,
                                onValueChange = { mutableSet.reps = it },
                                label = { Text("Reps") },
                                modifier = Modifier
                                    .padding(vertical = 8.dp)
                                    .weight(1f),
                                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                                shape = CircleShape
                            )

                            Spacer(modifier = Modifier.width(8.dp))

                            OutlinedTextField(
                                value = mutableSet.weight,
                                onValueChange = { mutableSet.weight = it },
                                label = { Text("Weight") },
                                modifier = Modifier
                                    .padding(vertical = 8.dp)
                                    .weight(1f),
                                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                                shape = CircleShape
                            )
                        }

                        OutlinedTextField(
                            value = mutableSet.technique,
                            onValueChange = { mutableSet.technique = it },
                            label = { Text("Technique") },
                            modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp),
                            shape = CircleShape
                        )

                        OutlinedTextField(
                            value = mutableSet.note,
                            onValueChange = { mutableSet.note = it },
                            label = { Text("Note") },
                            modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp),
                            shape = CircleShape
                        )
                    }
                },
                dismissButton = {
                    Button(
                        onClick = {
                            showAddSetPopup = false
                        }
                    ) {
                        Text("Cancel")
                    }
                },
                confirmButton = {
                    Button(
                        onClick = {
                            if(mutableSet.weight.isNotEmpty() and mutableSet.reps.isNotEmpty()) {
                                showAddSetPopup = false
                                userExercise.sets.add(mutableSet.toNonMutable())
                            }
                        },
                        colors = ButtonDefaults.buttonColors(Color.Green)
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Check,
                            contentDescription = "Change",
                            tint = Color.White
                        )
                    }
                },
                modifier = Modifier
                    .width(300.dp)
            )
        }
    }

    if(trainingDay.userExercises.isEmpty()) {
        Row (
            modifier = Modifier
                .padding(32.dp)
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = "No exercises were added yet.",
                fontSize = 16.sp,
                textAlign = TextAlign.Center,
            )
        }
    }

    Spacer(modifier = Modifier.height(16.dp))
}
