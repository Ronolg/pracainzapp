package com.example.gainitapp.ui.theme

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.*
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.gainitapp.ui.theme.account.AccountScreen
import com.example.gainitapp.ui.theme.exercises.ExercisesScreen
import com.example.gainitapp.ui.theme.home.HomeScreen
import com.example.gainitapp.ui.theme.settings.SettingsScreen
import com.example.gainitapp.ui.theme.trainingPlans.AddTrainingPlanScreen
import com.example.gainitapp.ui.theme.trainingPlans.TrainingPlanScreen
import com.example.gainitapp.ui.theme.trainingPlans.TrainingPlansScreen
import com.example.gainitapp.ui.theme.workouts.AddWorkoutScreen
import com.example.gainitapp.ui.theme.workouts.WorkoutScreen
import com.example.gainitapp.ui.theme.workouts.WorkoutsScreen

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun SetupNetGraph(navController: NavHostController) {
    NavHost(navController = navController, startDestination = Screen.HomeScreen.route) {
        // HOME
        composable(route = Screen.HomeScreen.route) {
            HomeScreen(navController)
        }

        // ACCOUNT
        composable(route = Screen.AccountScreen.route) {
            AccountScreen(navController)
        }

        // TRAINING PLAN
        composable(
            route = Screen.TrainingPlanScreen.route + "/{id}",
            arguments = listOf(
                navArgument("id") {
                    type = NavType.LongType
                }
            )
        ) {backStackEntry ->
            val viewModel = hiltViewModel<MainViewModel>()
            val trainingPlanId = backStackEntry.arguments?.getLong("id")
            TrainingPlanScreen(navController, viewModel, trainingPlanId)
        }

        // TRAINING PLANS
        composable(route = Screen.TrainingPlansScreen.route) {
            val viewModel = hiltViewModel<MainViewModel>()
            TrainingPlansScreen(navController, viewModel)
        }

        // ADD TRAINING PLAN
        composable(route = Screen.AddTrainingPlanScreen.route) {
            val viewModel = hiltViewModel<MainViewModel>()
            AddTrainingPlanScreen(navController, viewModel)
        }

        // WORKOUTS
        composable(route = Screen.WorkoutsScreen.route) {
            val viewModel = hiltViewModel<MainViewModel>()
            WorkoutsScreen(navController, viewModel)
        }

        // SINGLE WORKOUT
        composable(
            route = Screen.WorkoutScreen.route + "/{id}",
            arguments = listOf(
                navArgument("id") {
                    type = NavType.LongType
                //                    type = NavType.SerializableType(Workout::class.java)
                }
            )
        ) { backStackEntry ->
            val viewModel = hiltViewModel<MainViewModel>()
            val workoutId = backStackEntry.arguments?.getLong("id")
            WorkoutScreen(navController = navController, viewModel, workoutId = workoutId)
        }

        // ADD WORKOUT
        composable(route = Screen.AddWorkoutScreen.route) {
            val viewModel = hiltViewModel<MainViewModel>()
            AddWorkoutScreen(navController, viewModel)
        }

        // EXERCISES
        composable(route = Screen.ExercisesScreen.route) {
            val viewModel = hiltViewModel<MainViewModel>()
            ExercisesScreen(navController, viewModel)
        }

        // SETTINGS
        composable(route = Screen.SettingsScreen.route) {
            SettingsScreen(navController)
        }
    }
}

