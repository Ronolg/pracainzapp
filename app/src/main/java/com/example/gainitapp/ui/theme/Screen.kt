package com.example.gainitapp.ui.theme

sealed class Screen(val route: String) {
    object HomeScreen : Screen("home_screen")
    object AccountScreen : Screen("account_screen")
    object TrainingPlanScreen : Screen("training_plan_screen")
    object TrainingPlansScreen : Screen("training_plans_screen")
    object AddTrainingPlanScreen : Screen("add_training_plans_screen")
    object WorkoutsScreen : Screen("workouts_screen")
    object WorkoutScreen : Screen("single_workout_screen")
    object AddWorkoutScreen : Screen("add_workout_screen")
    object ExercisesScreen: Screen("exercises_screen")
    object SettingsScreen : Screen("settings_screen")

    fun withArgs(vararg args: String): String {
        return buildString {
            append(route)
            args.forEach { arg ->
                append("/$arg")
            }
        }
    }
}