package com.example.gainitapp.ui.theme.workouts

import android.annotation.SuppressLint
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.model.Exercise
import com.example.gainitapp.data.remote.model.Workout
import com.example.gainitapp.ui.theme.MainViewModel
import com.example.gainitapp.ui.theme.NavigationDrawer
import com.example.gainitapp.ui.theme.Screen
import com.example.gainitapp.ui.theme.exercises.ExerciseList

@OptIn(ExperimentalMaterial3Api::class)
@RequiresApi(Build.VERSION_CODES.O)
@SuppressLint("StateFlowValueCalledInComposition", "UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun WorkoutsScreen(navController: NavHostController, viewModel: MainViewModel) {

    val userWorkoutsState = viewModel.getWorkouts().collectAsState()

    NavigationDrawer(navController) {
        Scaffold (
            floatingActionButton = {
                FloatingActionButton(
                    onClick = {
                        navController.navigate(Screen.AddWorkoutScreen.route)
                    },
                    containerColor = MaterialTheme.colorScheme.primaryContainer,
                    contentColor = MaterialTheme.colorScheme.primary,
                    shape = CircleShape,
                ) {
                    Icon(Icons.Filled.Add, "Add new workout")
                }
            },
            floatingActionButtonPosition = FabPosition.End
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                    contentAlignment = Alignment.Center
                ) {
                    Text(text = "My workouts", fontSize = 20.sp)
                }

                when (userWorkoutsState.value) {
                    is Resource.Success -> {
                        val userWorkouts = (userWorkoutsState.value as Resource.Success<List<Workout>>).result
                        Box(modifier = Modifier.fillMaxWidth()) {
                            WorkoutsList(navController, userWorkouts, viewModel)
                        }
                    }
                    is Resource.Failure -> {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth(),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(text = "No workouts found", fontSize = 16.sp)
                        }
                    }

                    Resource.Loading -> {}

                    null -> {}
                }
            }
        }



    }
}