package com.example.gainitapp.ui.theme.workouts

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.model.Workout
import com.example.gainitapp.ui.theme.MainViewModel
import com.example.gainitapp.ui.theme.NavigationDrawer
import com.example.gainitapp.ui.theme.userexercises.UserExerciseList

@Composable
fun WorkoutScreen(navController: NavHostController, viewModel: MainViewModel, workoutId: Long?) {

    val workoutState = workoutId?.let { viewModel.getWorkoutById(it).collectAsState() }
    val scrollState = rememberScrollState()

    NavigationDrawer(navController) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
//                .verticalScroll(scrollState)
        ) {
            if (workoutState != null) {
                when (workoutState.value) {
                    is Resource.Success -> {
                        val workout = (workoutState.value as Resource.Success<Workout>).result
                        UserExerciseList(workout.userExercises)
                    }

                    is Resource.Failure -> {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth(),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(text = "Workout was not found", fontSize = 16.sp)
                        }
                    }

                    Resource.Loading -> {}

                    null -> {}
                }
            }
        }
    }
}