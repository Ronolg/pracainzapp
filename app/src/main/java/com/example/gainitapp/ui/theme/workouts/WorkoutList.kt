package com.example.gainitapp.ui.theme.workouts

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import com.example.gainitapp.data.remote.model.Workout
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import com.example.gainitapp.ui.theme.MainViewModel

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun WorkoutsList(navController: NavHostController, workouts: List<Workout>, viewModel: MainViewModel) {

    Box(modifier = Modifier.fillMaxWidth()) {
        LazyColumn {
            items(workouts) {
                    workout ->
                WorkoutItem(workout = workout, navController, viewModel)
            }
        }
    }
}

//@Preview(showBackground = true)
//@Composable
//fun PreviewWorkoutList() {
//    val navController = rememberNavController()
//    WorkoutsList(navController, initializeSampleWorkouts())
//}