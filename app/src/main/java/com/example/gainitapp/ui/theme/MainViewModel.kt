package com.example.gainitapp.ui.theme

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.dtos.TrainingPlanDTO
import com.example.gainitapp.data.remote.dtos.WorkoutDTO
import com.example.gainitapp.data.remote.model.Exercise
import com.example.gainitapp.data.remote.model.TrainingPlan
import com.example.gainitapp.data.remote.model.Workout
import com.example.gainitapp.data.remote.service.ExerciseService
import com.example.gainitapp.data.remote.service.TrainingPlanService
import com.example.gainitapp.data.remote.service.WorkoutService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val exerciseService: ExerciseService,
    private val workoutService: WorkoutService,
    private val trainingPlanService: TrainingPlanService,
) : ViewModel() {

    private val _exercises = MutableStateFlow<Resource<List<Exercise>>?>(null)
    private val exercises: StateFlow<Resource<List<Exercise>>?> = _exercises

    private val _workouts = MutableStateFlow<Resource<List<Workout>>?>(null)
    private val workouts: StateFlow<Resource<List<Workout>>?> = _workouts

    private val _workout = MutableStateFlow<Resource<Workout>?>(null)
    private val workout: StateFlow<Resource<Workout>?> = _workout

    private val _trainingPlans = MutableStateFlow<Resource<List<TrainingPlan>>?>(null)
    private val trainingPlans: StateFlow<Resource<List<TrainingPlan>>?> = _trainingPlans

    private val _trainingPlan = MutableStateFlow<Resource<TrainingPlan>?>(null)
    private val trainingPlan: StateFlow<Resource<TrainingPlan>?> = _trainingPlan

    // EXERCISES
    fun getExercises() : StateFlow<Resource<List<Exercise>>?> {
        viewModelScope.launch {
            _exercises.value = exerciseService.getExercises()
        }

        return exercises
    }

    fun addExercise(exercise: Exercise) {
        viewModelScope.launch {
              exerciseService.addExercise(exercise)
        }
    }

    // WORKOUT
    fun getWorkouts() : StateFlow<Resource<List<Workout>>?> {
        viewModelScope.launch {
            _workouts.value = workoutService.getWorkoutsForUser()
        }

        return workouts
    }

    fun getWorkoutById(workoutId: Long) : StateFlow<Resource<Workout>?> {
        viewModelScope.launch {
            _workout.value = workoutService.getWorkoutByIdForUser(workoutId)
        }

        return workout
    }

    fun addWorkout(workoutDTO: WorkoutDTO) {
        viewModelScope.launch {
            workoutService.addWorkout(workoutDTO)
        }
    }

    fun deleteWorkout(workoutId: Long) {
        viewModelScope.launch {
            workoutService.deleteWorkout(workoutId)
        }
    }

    // TRAINING PLAN
    fun getTrainingPlans() : StateFlow<Resource<List<TrainingPlan>>?> {
        viewModelScope.launch {
            _trainingPlans.value = trainingPlanService.getTrainingPlans()
        }

        return trainingPlans
    }

    fun getCurrentTrainingPlans() : StateFlow<Resource<TrainingPlan>?> {
        viewModelScope.launch {
            _trainingPlan.value = trainingPlanService.getCurrentTrainingPlan()
        }

        return trainingPlan
    }

    fun getTrainingPlanById(trainingPlanId: Long) : StateFlow<Resource<TrainingPlan>?> {
        viewModelScope.launch {
            _trainingPlan.value = trainingPlanService.getTrainingPlanById(trainingPlanId)
        }

        return trainingPlan
    }

    fun deleteTrainingPlan(trainingPlanId: Long) {
        viewModelScope.launch {
            trainingPlanService.deleteTrainingPlan(trainingPlanId)
        }
    }

    fun addTrainingPlan(trainingPlanDTO: TrainingPlanDTO) {
        viewModelScope.launch {
            trainingPlanService.addTrainingPlan(trainingPlanDTO)
        }
    }
}
