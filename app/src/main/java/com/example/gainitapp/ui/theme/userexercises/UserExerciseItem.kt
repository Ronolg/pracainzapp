package com.example.gainitapp.ui.theme.userexercises

import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.gainitapp.data.remote.model.UserExercise

@Composable
fun UserExerciseItem(userExercise: UserExercise) {
    var expanded by remember { mutableStateOf(false) }

    var setDetailsPopup by rememberSaveable {
        mutableStateOf(false)
    }

    val interactionSource = remember {
        MutableInteractionSource()
    }

    var itemHeight by remember {
        mutableStateOf(0.dp)
    }

    val density = LocalDensity.current
    val scrollState = rememberScrollState()

    Card(
//        elevation = CardDefaults.cardElevation(
//            defaultElevation = 6.dp
//        ),
        modifier = Modifier
            .padding(4.dp)
            .clickable { expanded = !expanded }
            .onSizeChanged {
                itemHeight = with(density) { it.height.toDp() }
            },
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        )
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .indication(interactionSource, LocalIndication.current)
                .padding(8.dp)
        ) {
            Text(text = userExercise.name)
        }

//        Column(modifier = Modifier.padding(horizontal = 16.dp)) {
            if (expanded) {
                var setNumber = 1
                userExercise.sets.forEach { set ->
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .indication(interactionSource, LocalIndication.current)
                            .clickable (
                                onClick = {
                                    setDetailsPopup = true
                                }
                            ).padding(horizontal = 8.dp)
                    ) {
                        Text(text = "Set $setNumber: ${set.weight}x${set.reps}, ${set.technique}")

                        if (setDetailsPopup) {
                            AlertDialog(
                                onDismissRequest = { setDetailsPopup = false },
                                title = { Text(text = "Set details", textAlign = TextAlign.Center) },
                                text = {
                                    Column(
                                        modifier = Modifier
                                            .verticalScroll(scrollState)
                                            .padding(8.dp)) {

                                        Text(text = "Reps: ${set.reps}")
                                        Text(text = "Weight: ${set.weight}")
                                        Text(text = "Technique: ${set.technique}")
                                        Text(text = "Note: ${set.note}")
                                    }
                                },
                                confirmButton = {
                                    // Empty button to avoid showing any action button
                                },
                                dismissButton = {
                                    // Empty button to avoid showing any action button
                                },
                                modifier = Modifier
                                    .width(300.dp)
                            )
                        }
                    }
                    setNumber++
                }
            }
//        }
    }


}
