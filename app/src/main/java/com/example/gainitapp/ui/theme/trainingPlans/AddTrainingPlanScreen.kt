package com.example.gainitapp.ui.theme.trainingPlans

import android.annotation.SuppressLint
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.gainitapp.data.remote.dtos.TrainingPlanDTO
import com.example.gainitapp.data.remote.model.TrainingDay
import com.example.gainitapp.data.remote.model.TrainingPlan
import com.example.gainitapp.data.remote.model.UserExercise
import com.example.gainitapp.ui.theme.MainViewModel
import com.example.gainitapp.ui.theme.NavigationDrawer
import com.example.gainitapp.ui.theme.Screen


@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("StateFlowValueCalledInComposition", "UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun AddTrainingPlanScreen(navController: NavHostController, viewModel: MainViewModel) {
    var confirmCreationPopup by remember { mutableStateOf(false) }

    var showAddExercisePopup by rememberSaveable {
        mutableStateOf(false)
    }

    var trainingDays = remember {
        mutableStateListOf<TrainingDay>()
    }

    NavigationDrawer(navController) {
        Scaffold (
            bottomBar = {
                Row (
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(32.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                )
                {
                    FloatingActionButton(
                        onClick = {
                            trainingDays.add(TrainingDay(userExercises = mutableListOf()))
                        },
                        containerColor = MaterialTheme.colorScheme.primaryContainer,
                        contentColor = MaterialTheme.colorScheme.primary,
                        shape = CircleShape,
                    ) {
                        Text(text = "Add new day", modifier = Modifier.padding(vertical = 16.dp, horizontal = 32.dp))
                    }

                    FloatingActionButton(
                        onClick = { confirmCreationPopup = true },
                        containerColor = MaterialTheme.colorScheme.primaryContainer,
                        contentColor = MaterialTheme.colorScheme.primary,
                        shape = CircleShape,
                    ) {
                        Icon(Icons.Filled.Check, "Add this training plan")
                    }
                }
            }
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)
                    .verticalScroll(rememberScrollState())
            ) {
                var dayNumber = 1
                trainingDays.forEach { trainingDay ->

                    TrainingDayItemCreator(trainingDay, dayNumber) {
                        showAddExercisePopup = true
                    }

                    dayNumber++

                    if(showAddExercisePopup) {
                        var newExerciseName by remember { mutableStateOf("") }

                        AlertDialog(
                            onDismissRequest = { showAddExercisePopup = false },
                            title = { Text(text = "Add new exercise", color = MaterialTheme.colorScheme.primary)},
                            text = {
                                Column(
                                    modifier = Modifier
                                        .padding(8.dp)) {

                                    OutlinedTextField(
                                        value = newExerciseName,
                                        onValueChange = { newExerciseName = it },
                                        label = { Text("Exercise name") },
                                        modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp),
                                        shape = CircleShape
                                    )
                                }
                            },
                            dismissButton = {
                                Button(
                                    onClick = {
                                        showAddExercisePopup = false
                                    }
                                ) {
                                    Text("Cancel")
                                }
                            },
                            confirmButton = {
                                Button(
                                    onClick = {
                                        showAddExercisePopup = false
                                        trainingDay.userExercises.add(UserExercise(name = newExerciseName))
                                    }
                                ) {
                                    Text("Add")
                                }
                            },
                            modifier = Modifier
                                .width(300.dp)
                        )
                    }
                }

                if(trainingDays.isEmpty()) {
                    Row (
                        modifier = Modifier
                            .padding(32.dp)
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = "No training days were added\n click below to do so.",
                            fontSize = 16.sp,
                            textAlign = TextAlign.Center,
                            maxLines = 2
                        )
                    }
                }

                if (confirmCreationPopup) {

                    var trainingPlanName by remember { mutableStateOf("")}
                    var trainingPlanNote by remember { mutableStateOf("")}
                    var isCurrent by remember { mutableStateOf(false) }

                    AlertDialog(
                        onDismissRequest = { confirmCreationPopup = false },
                        title = { Text(text = "Create the plan") },
                        text = {
                            Column() {
                                OutlinedTextField(
                                    value = trainingPlanName,
                                    onValueChange = {trainingPlanName = it },
                                    label = { Text("Plan name") },
                                    modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp),
                                    shape = CircleShape
                                )

                                OutlinedTextField(
                                    value = trainingPlanNote,
                                    onValueChange = {trainingPlanNote = it },
                                    label = { Text("Plan note") },
                                    modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp),
                                    shape = CircleShape
                                )

                                Row(verticalAlignment = Alignment.CenterVertically,
                                    modifier = Modifier.padding(8.dp)) {
                                    Text(
                                        text = "Do you want to make this workout your current one?",
                                        modifier = Modifier.weight(4f)
                                    )

                                    Switch(
                                        modifier = Modifier.weight(1f),
                                        checked = isCurrent,
                                        onCheckedChange = {
                                            isCurrent = it
                                        }
                                    )
                                }
                            }
                        },
                        dismissButton = {
                            Button(
                                onClick = {
                                    confirmCreationPopup = false
                                },
                                colors = ButtonDefaults.buttonColors(Color.Red)
                            ) {
                                Icon(
                                    imageVector = Icons.Filled.Close,
                                    contentDescription = "Change",
                                    tint = Color.White
                                )
                            }
                        },
                        confirmButton = {
                            Button(
                                onClick = {
                                    confirmCreationPopup = false

                                    viewModel.addTrainingPlan(
                                        TrainingPlan(
                                            current = isCurrent,
                                            name = trainingPlanName,
                                            note = trainingPlanNote,
                                            trainingDays = trainingDays
                                        ).toDto())

                                    navController.navigate(Screen.TrainingPlansScreen.route)
                                },
                                colors = ButtonDefaults.buttonColors(Color.Green)
                            ) {
                                Icon(
                                    imageVector = Icons.Filled.Check,
                                    contentDescription = "Change",
                                    tint = Color.White
                                )
                            }
                        },
                        modifier = Modifier
                            .width(300.dp)
                    )
                }
            }
        }
    }
}
