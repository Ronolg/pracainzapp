package com.example.gainitapp.ui.theme.exercises

import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import com.example.gainitapp.data.remote.model.Exercise

@Composable
fun ExerciseItem(exercise: Exercise) {

    var isExercisePopupVisible by rememberSaveable {
        mutableStateOf(false)
    }

    var itemHeight by remember {
        mutableStateOf(0.dp)
    }

    val interactionSource = remember {
        MutableInteractionSource()
    }

    val density = LocalDensity.current
    val scrollState = rememberScrollState()

    Card(
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        ),
        modifier = Modifier
            .onSizeChanged {
                itemHeight = with(density) { it.height.toDp() }
            }
            .padding(vertical = 4.dp),
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        )
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .indication(interactionSource, LocalIndication.current)
                .clickable (
                    onClick = {
                        isExercisePopupVisible = true
                    }
                )
                .padding(8.dp)
        ) {
            Text(text = exercise.name, color = MaterialTheme.colorScheme.primary)
        }

        if (isExercisePopupVisible) {
            AlertDialog(
                onDismissRequest = { isExercisePopupVisible = false },
                title = { Text(text = exercise.name) },
                text = {
                    Column(
                        modifier = Modifier
                            .verticalScroll(scrollState)
                            .padding(8.dp)) {

                                Text(
                                    text = exercise.description,
                                    modifier = Modifier.padding(16.dp)
                                )
                            }
                    },
                confirmButton = {
                    // Empty button to avoid showing any action button
                },
                dismissButton = {
                    // Empty button to avoid showing any action button
                },
                modifier = Modifier
                    .width(300.dp)
            )
        }
    }


}
