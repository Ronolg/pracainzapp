package com.example.gainitapp.ui.theme.trainingPlans

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.model.TrainingPlan
import com.example.gainitapp.ui.theme.MainViewModel
import com.example.gainitapp.ui.theme.NavigationDrawer
import com.example.gainitapp.ui.theme.Screen


@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun TrainingPlansScreen(navController: NavHostController, viewModel: MainViewModel) {

    val currentTrainingPlanState = viewModel.getCurrentTrainingPlans().collectAsState()
    val trainingPlansState = viewModel.getTrainingPlans().collectAsState()

    NavigationDrawer(navController) {
        Scaffold (
            floatingActionButton = {
                FloatingActionButton(
                    onClick = {
                          navController.navigate(Screen.AddTrainingPlanScreen.route)
                    },
                    containerColor = MaterialTheme.colorScheme.primaryContainer,
                    contentColor = MaterialTheme.colorScheme.primary,
                    shape = CircleShape,
                ) {
                    Icon(Icons.Filled.Add, "Add new training plan")
                }
            },
            floatingActionButtonPosition = FabPosition.End
        ) {
            Column (
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)
//                    .verticalScroll(rememberScrollState())
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                    contentAlignment = Alignment.Center
                ) {
                    Text(text = "Current training plan", fontSize = 20.sp)
                }

                when (currentTrainingPlanState.value) {
                    is Resource.Success -> {
                        val currentTrainingPlan = (currentTrainingPlanState.value as Resource.Success<TrainingPlan>).result
                        Box(modifier = Modifier.fillMaxWidth()) {
                            TrainingPlanItem(navController, currentTrainingPlan, viewModel)
                        }
                    }
                    is Resource.Failure -> {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth(),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(text = "You do not have current training plan yet.", fontSize = 16.sp)
                        }
                    }

                    Resource.Loading -> {}

                    null -> {}
                }

                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                    contentAlignment = Alignment.Center
                ) {
                    Text(text = "My training plans", fontSize = 20.sp)
                }

                when (trainingPlansState.value) {
                    is Resource.Success -> {
                        val trainingPlans = (trainingPlansState.value as Resource.Success<List<TrainingPlan>>).result
                        Box(modifier = Modifier.fillMaxWidth()) {
                            TrainingPlanList(navController, trainingPlans, viewModel)
                        }
                    }
                    is Resource.Failure -> {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth(),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(text = "You do not have any training plans yet.", fontSize = 16.sp)
                        }
                    }

                    Resource.Loading -> {}

                    null -> {}
                }
            }
        }
    }
}
