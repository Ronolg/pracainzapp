package com.example.gainitapp.data.remote.model

import com.example.gainitapp.data.remote.dtos.SetDTO
import com.example.gainitapp.data.remote.dtos.UserExerciseDTO
import kotlinx.serialization.Serializable

@Serializable
data class UserExercise (
    val id: Long = -1,
    var name: String = "",
    val sets: MutableList<Set> = mutableListOf()
) {
    fun toDto(): UserExerciseDTO
    {
        var setDtos = mutableListOf<SetDTO>()

        sets.forEach {set ->
            setDtos.add(set.toDto())
        }

        return UserExerciseDTO(
            name = name,
            sets = setDtos
        )
    }
}
