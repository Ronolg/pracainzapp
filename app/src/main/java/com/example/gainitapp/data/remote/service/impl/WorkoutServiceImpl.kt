package com.example.gainitapp.data.remote.service.impl

import com.example.gainitapp.data.remote.HttpRoutes
import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.dtos.WorkoutDTO
import com.example.gainitapp.data.remote.model.Exercise
import com.example.gainitapp.data.remote.model.Workout
import com.example.gainitapp.data.remote.service.WorkoutService
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.network.sockets.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import javax.inject.Inject

class WorkoutServiceImpl @Inject constructor(
    private val client: HttpClient
) : WorkoutService
{
    override suspend fun getWorkoutsForUser(): Resource<List<Workout>> {
        return try {
            val response = client.get(HttpRoutes.USER_WORKOUTS + "all")

            if(response.status == HttpStatusCode.NoContent) {
                Resource.Success(emptyList())
            } else
            {
                Resource.Success(response.body<List<Workout>>())
            }
        } catch(e: RedirectResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ClientRequestException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ServerResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: HttpRequestTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: ConnectTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        }
    }

    override suspend fun getWorkoutByIdForUser(workoutId: Long): Resource<Workout> {
        return try {
            Resource.Success(
                client.get(HttpRoutes.USER_WORKOUTS + workoutId).body<Workout>()
            )
        } catch(e: RedirectResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ClientRequestException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ServerResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: HttpRequestTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: ConnectTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        }
    }

    override suspend fun addWorkout(workoutDTO: WorkoutDTO): Resource<String> {
        return try {
            Resource.Success(
                client.post(HttpRoutes.USER_WORKOUTS_POST) {
                    contentType(ContentType.Application.Json)
                    setBody(workoutDTO)
                }.body<String>()
            )
        } catch(e: RedirectResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ClientRequestException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ServerResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        }  catch (e: HttpRequestTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: ConnectTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        }
    }

    override suspend fun deleteWorkout(workoutId: Long): Resource<String> {
        return try {
            Resource.Success(
                client.delete(HttpRoutes.USER_WORKOUTS_DELETE + workoutId) {
                    contentType(ContentType.Application.Json)
                }.body<String>()
            )
        } catch(e: RedirectResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ClientRequestException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ServerResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        }  catch (e: HttpRequestTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: ConnectTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        }
    }
}
