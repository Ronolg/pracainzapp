package com.example.gainitapp.data.remote.service

import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.dtos.WorkoutDTO
import com.example.gainitapp.data.remote.model.Workout

interface WorkoutService {
    suspend fun getWorkoutsForUser(): Resource<List<Workout>>
    suspend fun getWorkoutByIdForUser(workoutId: Long): Resource<Workout>
    suspend fun addWorkout(workoutDTO: WorkoutDTO): Resource<String>
    suspend fun deleteWorkout(workoutId: Long): Resource<String>
}
