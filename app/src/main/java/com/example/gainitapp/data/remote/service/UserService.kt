package com.example.gainitapp.data.remote.service

import com.example.gainitapp.data.remote.model.User

interface UserService {
    suspend fun registerUser(user: User): User
}