package com.example.gainitapp.data.remote.model

import com.example.gainitapp.data.remote.dtos.SetDTO
import kotlinx.serialization.Serializable

@Serializable
data class Set (
    var id: Long = -1,
    var reps: Int = 0,
    var weight: Float = 0f,
    var technique: String = "",
    var note: String = "",
    var dropset: Boolean = false
) {
    fun toDto(): SetDTO
    {
        return SetDTO(
            reps = reps,
            weight = weight,
            technique = technique,
            note = note,
            dropset = dropset
        )
    }
}
