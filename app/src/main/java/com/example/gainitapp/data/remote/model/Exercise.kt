package com.example.gainitapp.data.remote.model

import kotlinx.serialization.Serializable

@Serializable
data class Exercise (
    val name: String,
    val description: String
)
