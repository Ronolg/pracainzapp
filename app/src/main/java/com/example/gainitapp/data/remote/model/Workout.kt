package com.example.gainitapp.data.remote.model

import com.example.gainitapp.data.remote.dtos.UserExerciseDTO
import com.example.gainitapp.data.remote.dtos.WorkoutDTO
import kotlinx.serialization.Serializable

@Serializable
data class Workout (
    val id: Long = -1,
    val date: String,
    val note: String,
    val userExercises: List<UserExercise>
) {
    fun toDto() : WorkoutDTO
    {
        var userExercisesDtos = mutableListOf<UserExerciseDTO>()

        userExercises.forEach {userExercise ->
            userExercisesDtos.add(userExercise.toDto())
        }

        return WorkoutDTO(
            date = date,
            note = note,
            userExercises = userExercisesDtos
        )
    }
}
