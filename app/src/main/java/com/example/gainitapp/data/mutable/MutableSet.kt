package com.example.gainitapp.data.mutable

import androidx.compose.runtime.*
import com.example.gainitapp.data.remote.model.Set

class MutableSet {
    var reps by mutableStateOf("")
    var weight by mutableStateOf("")
    var technique by mutableStateOf("")
    var note by mutableStateOf("")
    var dropset by mutableStateOf(false)

    fun toNonMutable(): Set {
        return Set(
            reps = reps.toInt(),
            weight = weight.toFloat(),
            technique = technique,
            note = note,
            dropset = dropset)
    }
}
