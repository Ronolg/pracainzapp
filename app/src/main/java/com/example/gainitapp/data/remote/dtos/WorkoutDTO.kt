package com.example.gainitapp.data.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class WorkoutDTO(
    val date: String,
    val note: String,
    val userExercises: List<UserExerciseDTO>
)
