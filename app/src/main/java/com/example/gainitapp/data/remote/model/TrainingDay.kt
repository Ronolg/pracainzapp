package com.example.gainitapp.data.remote.model

import com.example.gainitapp.data.remote.dtos.TrainingDayDTO
import com.example.gainitapp.data.remote.dtos.UserExerciseDTO
import kotlinx.serialization.Serializable

@Serializable
data class TrainingDay (
    val id: Long = -1,
    val userExercises: MutableList<UserExercise>
) {
    fun toDto() : TrainingDayDTO
    {
        var userExercisesDtos = mutableListOf<UserExerciseDTO>()

        userExercises.forEach {userExercise ->
            userExercisesDtos.add(userExercise.toDto())
        }

        return TrainingDayDTO(
            userExercises = userExercisesDtos
        )
    }
}
