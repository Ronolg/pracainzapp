package com.example.gainitapp.data.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class SetDTO(
    val reps: Int,
    val weight: Float,
    val technique: String,
    val note: String,
    val dropset: Boolean
)
