package com.example.gainitapp.data.remote.model

import com.example.gainitapp.data.remote.dtos.TrainingDayDTO
import com.example.gainitapp.data.remote.dtos.TrainingPlanDTO
import kotlinx.serialization.Serializable

@Serializable
data class TrainingPlan (
    val id: Long = -1,
    val current: Boolean,
    val name: String,
    val note: String,
    val trainingDays: MutableList<TrainingDay>
) {
    fun toDto() : TrainingPlanDTO
    {
        var trainingDaysDtos = mutableListOf<TrainingDayDTO>()

        trainingDays.forEach {trainingDay ->
            trainingDaysDtos.add(trainingDay.toDto())
        }

        return TrainingPlanDTO(
            current = current,
            name = name,
            note = note,
            trainingDays = trainingDaysDtos
        )
    }
}
