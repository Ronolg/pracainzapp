package com.example.gainitapp.data.remote.dtos

import com.example.gainitapp.data.remote.model.TrainingDay
import kotlinx.serialization.Serializable

@Serializable
data class TrainingPlanDTO(
    val current: Boolean,
    val name: String,
    val note: String,
    val trainingDays: List<TrainingDayDTO>
)
