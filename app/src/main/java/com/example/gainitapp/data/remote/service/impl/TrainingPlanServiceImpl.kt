package com.example.gainitapp.data.remote.service.impl

import com.example.gainitapp.data.remote.HttpRoutes
import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.dtos.TrainingPlanDTO
import com.example.gainitapp.data.remote.dtos.WorkoutDTO
import com.example.gainitapp.data.remote.exception.TrainingPlanNotFoundException
import com.example.gainitapp.data.remote.model.TrainingPlan
import com.example.gainitapp.data.remote.service.TrainingPlanService
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.network.sockets.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import javax.inject.Inject


class TrainingPlanServiceImpl @Inject constructor (
    private val client: HttpClient
): TrainingPlanService {
    override suspend fun getCurrentTrainingPlan(): Resource<TrainingPlan> {
        return try {
            val response = client.get(HttpRoutes.USER_TRAINING_PLAN + "get/current")

            if(response.status.isSuccess() and (response.status != HttpStatusCode.NoContent)){
                Resource.Success(response.body<TrainingPlan>())
            } else {
                Resource.Failure(TrainingPlanNotFoundException())
            }

        } catch(e: RedirectResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ClientRequestException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ServerResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: HttpRequestTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: ConnectTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        }
    }

    override suspend fun getTrainingPlanById(trainingPlanId: Long): Resource<TrainingPlan> {
        return try {
            val response = client.get(HttpRoutes.USER_TRAINING_PLAN + "get/$trainingPlanId")

            if(response.status.isSuccess() and (response.status != HttpStatusCode.NoContent)){
                Resource.Success(response.body<TrainingPlan>())
            } else {
                Resource.Failure(TrainingPlanNotFoundException())

            }
        } catch(e: RedirectResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ClientRequestException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ServerResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: HttpRequestTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: ConnectTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        }
    }

    override suspend fun getTrainingPlans(): Resource<List<TrainingPlan>> {
        return try {

            val response = client.get(HttpRoutes.USER_TRAINING_PLAN + "get/all")

            if(response.status == HttpStatusCode.NoContent) {
                Resource.Success(emptyList())
            } else
            {
                Resource.Success(response.body<List<TrainingPlan>>())
            }

        } catch(e: RedirectResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ClientRequestException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ServerResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: HttpRequestTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: ConnectTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        }
    }

    override suspend fun deleteTrainingPlan(trainingPlanId: Long): Resource<String> {
        return try {
            Resource.Success(
                client.delete(HttpRoutes.USER_TRAINING_PLAN_DELETE + trainingPlanId) {
                    contentType(ContentType.Application.Json)
                }.body<String>()
            )
        } catch(e: RedirectResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ClientRequestException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ServerResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        }  catch (e: HttpRequestTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: ConnectTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        }
    }

    override suspend fun addTrainingPlan(trainingPlanDTO: TrainingPlanDTO): Resource<String> {
        return try {
            Resource.Success(
                client.post(HttpRoutes.USER_TRAINING_PLAN_POST) {
                    contentType(ContentType.Application.Json)
                    setBody(trainingPlanDTO)
                }.body<String>()
            )
        } catch(e: RedirectResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ClientRequestException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ServerResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        }  catch (e: HttpRequestTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: ConnectTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        }
    }
}
