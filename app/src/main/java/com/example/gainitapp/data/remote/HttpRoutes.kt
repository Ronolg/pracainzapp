package com.example.gainitapp.data.remote

object HttpRoutes {
    private const val userId = 32 // TEMPORARY

    private const val BASE_URL = "http://10.0.2.2:8080"

    const val EXERCISES_GET = "$BASE_URL/exercise/getAll"
    const val EXERCISE_POST = "$BASE_URL/exercise/add"

    const val USER_WORKOUTS = "$BASE_URL/user/$userId/getWorkout/"
    const val USER_WORKOUTS_POST = "$BASE_URL/user/$userId/addWorkout"
    const val USER_WORKOUTS_DELETE = "$BASE_URL/user/$userId/deleteWorkout/"

    const val USER_TRAINING_PLAN = "$BASE_URL/user/$userId/trainingPlan/"
    const val USER_TRAINING_PLAN_POST = "$BASE_URL/user/$userId/trainingPlan/add"
    const val USER_TRAINING_PLAN_DELETE = "$BASE_URL/user/$userId/deleteTrainingPlan/"
}