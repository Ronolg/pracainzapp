package com.example.gainitapp.data.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class TrainingDayDTO(
    val userExercises: List<UserExerciseDTO>
)
