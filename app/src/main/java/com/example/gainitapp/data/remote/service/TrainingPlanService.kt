package com.example.gainitapp.data.remote.service

import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.dtos.TrainingPlanDTO
import com.example.gainitapp.data.remote.model.TrainingPlan

interface TrainingPlanService {
    suspend fun getCurrentTrainingPlan(): Resource<TrainingPlan>
    suspend fun getTrainingPlanById(trainingPlanId: Long): Resource<TrainingPlan>
    suspend fun getTrainingPlans(): Resource<List<TrainingPlan>>
    suspend fun deleteTrainingPlan(trainingPlanId: Long): Resource<String>
    suspend fun addTrainingPlan(trainingPlanDTO: TrainingPlanDTO): Resource<String>
}