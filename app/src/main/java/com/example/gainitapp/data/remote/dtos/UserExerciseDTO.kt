package com.example.gainitapp.data.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class UserExerciseDTO(
    val name: String,
    val sets: List<SetDTO>
)
