package com.example.gainitapp.data.remote.exception

class TrainingPlanNotFoundException(
    message: String? = "Training plan not found",
    cause: Throwable? = null
) : Exception (message, cause)
