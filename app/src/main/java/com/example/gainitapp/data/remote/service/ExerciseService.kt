package com.example.gainitapp.data.remote.service

import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.service.impl.ExerciseServiceImpl
import com.example.gainitapp.data.remote.model.Exercise
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*

import io.ktor.client.plugins.logging.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json


interface ExerciseService {
    suspend fun getExercises(): Resource<List<Exercise>>
//    suspend fun getExercises(): List<Exercise>

    suspend fun addExercise(exercise: Exercise)//: Exercise?
}