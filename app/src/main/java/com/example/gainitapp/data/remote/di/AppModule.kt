package com.example.gainitapp.data.remote.di

import com.example.gainitapp.data.remote.AppHttpClient
import com.example.gainitapp.data.remote.service.ExerciseService
import com.example.gainitapp.data.remote.service.TrainingPlanService
import com.example.gainitapp.data.remote.service.WorkoutService
import com.example.gainitapp.data.remote.service.impl.ExerciseServiceImpl
import com.example.gainitapp.data.remote.service.impl.TrainingPlanServiceImpl
import com.example.gainitapp.data.remote.service.impl.WorkoutServiceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*


@InstallIn(SingletonComponent::class)
@Module
class AppModule {

    @Provides
    fun getHttpClient(httpClient: AppHttpClient): HttpClient = httpClient.getHttpclient()

    @Provides
    fun getExerciseService(impl: ExerciseServiceImpl): ExerciseService = impl

    @Provides
    fun getWorkoutService(impl: WorkoutServiceImpl): WorkoutService = impl

    @Provides
    fun getTrainingPlanService(impl: TrainingPlanServiceImpl): TrainingPlanService = impl

}