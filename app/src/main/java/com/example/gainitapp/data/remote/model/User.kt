package com.example.gainitapp.data.remote.model

import kotlinx.serialization.Serializable

@Serializable
data class User (
    val username: String,
    val email: String,
    val password: String
//    var authToken: String? = null
)
