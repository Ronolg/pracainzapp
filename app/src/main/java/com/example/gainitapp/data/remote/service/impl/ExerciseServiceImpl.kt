package com.example.gainitapp.data.remote.service.impl

import com.example.gainitapp.data.remote.HttpRoutes
import com.example.gainitapp.data.remote.Resource
import com.example.gainitapp.data.remote.model.Exercise
import com.example.gainitapp.data.remote.service.ExerciseService
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.network.sockets.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import javax.inject.Inject

class ExerciseServiceImpl @Inject constructor(
    private val client: HttpClient
) : ExerciseService {

    override suspend fun getExercises(): Resource<List<Exercise>> {
        return try {
            Resource.Success(
                client.get(HttpRoutes.EXERCISES_GET).body<List<Exercise>>())
        } catch(e: RedirectResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ClientRequestException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch(e: ServerResponseException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: HttpRequestTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        } catch (e: ConnectTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        }
    }

    override suspend fun addExercise(exercise: Exercise) {
        try {
            client.post(HttpRoutes.EXERCISE_POST) {
                contentType(ContentType.Application.Json)
                setBody(exercise)
            }
//                .body<Exercise>()
        } catch(e: RedirectResponseException) {
            e.printStackTrace()
        } catch(e: ClientRequestException) {
            e.printStackTrace()
        } catch(e: ServerResponseException) {
            e.printStackTrace()
        }  catch (e: HttpRequestTimeoutException) {
            e.printStackTrace()
        } catch (e: ConnectTimeoutException) {
            e.printStackTrace()
            Resource.Failure(e)
        }
    }
}

