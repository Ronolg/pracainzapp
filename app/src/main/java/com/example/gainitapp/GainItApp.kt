package com.example.gainitapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class GainItApp : Application() {
}